# -*- coding: utf-8 -*-

import datetime
from django.contrib.auth.models import User
from tastypie.test import ResourceTestCase
from elastic.country.models import *
from django.utils.encoding import smart_str

class UomResourceTest(ResourceTestCase):
    # Use ``fixtures`` & ``urls`` as normal. See Django's ``TestCase``
    # documentation for the gory details.
    fixtures = ['product/initial_data.json']

    def setUp(self):
        super(UomResourceTest, self).setUp()
        self.username = 'igor.kovalenko'
        self.password = ''

    def get_credentials(self):
        return self.create_basic(username=self.username, password=self.password)

    def test_get_list_unauthorzied(self):
        self.assertHttpUnauthorized(self.api_client.get('/api/v1/product/oum/', format='json'))

    def test_get_list_json(self):
        resp = self.api_client.get('/api/v1/product/oum/', format='json', authentication=self.get_credentials())
        self.assertValidJSONResponse(resp)

        # Scope out the data for correctness.
        self.assertEqual(len(self.deserialize(resp)['objects']), 20)

        # Here, we're checking an entire structure for the expected data.
        record = {
            'category': {
                'id': 7,
                'name': "Экономические единицы",
                'resource_uri': "/api/v1/product/uom_category/7/"
            },
            'code': 796,
            'form': "Международные",
            'id': 192,
            'international_symbol': "pc; 1",
            'name': "Штука",
            'resource_uri': "/api/v1/product/uom/796/",
            'symbol': "шт"
        }
        self.assertEqual(self.deserialize(resp)['objects'].count(record), 1)

    def test_get_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.get('/api/v1/product/uom/796/', format='json'))

    def test_get_detail_json(self):
        resp = self.api_client.get('/api/v1/product/uom/796/', format='json', authentication=self.get_credentials())
        self.assertValidJSONResponse(resp)

        # We use ``assertKeys`` here to just verify the keys, not all the data.
        self.assertKeys(self.deserialize(resp), ['category', 'code','form','id', 'international_symbol', 'name',
                                                 'resource_uri', 'symbol',])
        self.assertEqual(self.deserialize(resp)['code'], 796)


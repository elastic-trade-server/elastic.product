# -*- coding: utf-8 -*-

import django.dispatch

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"

product_group_changed = django.dispatch.Signal()
product_group_created = django.dispatch.Signal()
product_category_changed = django.dispatch.Signal()
product_category_created = django.dispatch.Signal()
replication_request = django.dispatch.Signal()

# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from tastypie.bundle import Bundle
from tastypie.exceptions import BadRequest, NotFound
from tastypie.utils.urls import trailing_slash
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from tastypie.utils import dict_strip_unicode_keys
from tastypie import http
from django.db import transaction
from django.utils.encoding import force_unicode
import signals
from django.core.serializers.json import DjangoJSONEncoder
from tastypie.serializers import Serializer
import json
from dateutil.parser import parse
from tastypie.utils import make_aware
from decimal import Decimal


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


class DojoRefSerializer(Serializer):
    def to_json(self, data, options=None):
        options = options or {}

        data = self.to_simple(data, options)

        if 'objects' in data:
            data = data['objects']

        return json.dumps(data, cls=DjangoJSONEncoder, sort_keys=True)


class RuntimeTastypieException(Exception):
    """
    Общая ошибка времени исполнения
    """
    def __init__(self, *args, **kwargs):
        super(RuntimeTastypieException, self).__init__(*args, **kwargs)

    def __str__(self):
        return force_unicode(self.args[0])

    def __repr__(self):
        return force_unicode(self.args[0])


class UomCategoryResource(ModelResource):
    class Meta:
        queryset = UomCategory.objects.all()
        resource_name = 'uom_category'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        include_resource_uri = False
        filtering = {
            "name": ALL,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))


class UomResource(ModelResource):
    class Meta:
        queryset = Uom.objects.all()
        resource_name = 'uom'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        allowed_methods = ['get', ]
        include_resource_uri = False
        excludes = ['id', ]
        filtering = {
            "name": ALL,
            "symbol": ALL,
            "category": ALL_WITH_RELATIONS,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    code = fields.IntegerField(attribute='code', help_text=_('Code'))
    symbol = fields.CharField(attribute='symbol', help_text=_('Symbol'))
    international_symbol = fields.CharField(attribute='international_symbol', help_text=_('International symbol'))
    form = fields.CharField(attribute='form', help_text=_('Form'))
    category = fields.ToOneField(UomCategoryResource, 'category', full=True, help_text=_('UOM category'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs


class ProductGroupResource(ModelResource):
    class Meta:
        queryset = ProductGroup.objects.all()
        resource_name = 'product_group'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', 'lft', 'level', 'tree_id', 'rght', ]
        filtering = {
            "name": ALL,
            "code": ALL,
            "parent": ALL_WITH_RELATIONS,
        }

    parent = fields.ToOneField('self', 'parent', null=True, blank=True, full=True, help_text=_('Parent'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    code = fields.CharField(attribute='code', help_text=_('Code'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    def hydrate(self, bundle):

        def func(data, parent):
            obj = ProductGroup.objects.get_or_create(
                code=data['code'],
                defaults={
                    "name": data['name'],
                    "code": data['code'],
                    "parent": parent
                }
            )

            obj = obj[0] if type(obj) == tuple else obj

            if 'parent' in data and type(data['parent']) == dict:
                obj.parent = func(data['parent'], obj)
            return obj

        if 'parent' in bundle.data and type(bundle.data['parent']) == dict:
            try:
                bundle.obj.parent = func(bundle.data['parent'], bundle.obj)
            except Exception as e:
                print unicode(e)
                raise Exception(e)

        bundle.data.pop('parent', None)
        return super(ProductGroupResource, self).hydrate(bundle)

    @transaction.atomic
    def obj_update(self, bundle, **kwargs):
        try:
            bundle.obj = ProductGroup.objects.get(code=kwargs['code'])
        except ObjectDoesNotExist:
            raise NotFound('Product group with code={0} not found!'.format(kwargs['code']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date and bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        bundle = super(ProductGroupResource, self).obj_update(bundle, **kwargs)

        # Отправляем сигнал "Определение группы товара изменилось"
        signals.product_group_changed.send_robust(sender=bundle.obj)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = ProductGroup.objects.get(code=bundle.data['code'])
        except ObjectDoesNotExist:
            pass
        else:
            kwargs['pk'] = bundle.obj.id
            kwargs['code'] = bundle.obj.code
            return self.obj_update(bundle, **kwargs)

        bundle = super(ProductGroupResource, self).obj_create(bundle, **kwargs)

        # Отправляем сигнал "Создана новая категория товара"
        signals.product_group_created.send_robust(sender=bundle.obj)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    def obj_delete(self, bundle, **kwargs):
        try:
            bundle.obj = ProductGroup.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj, **{'is_removing': True})
        bundle.obj.delete()

    @transaction.atomic
    def post_list(self, request, **kwargs):
        deserialized = self.deserialize(request, request.body,
                                        format=request.META.get('CONTENT_TYPE', 'application/json'))

        if type(deserialized) is not list:
            deserialized = [deserialized]

        def func(i, l):
            """
            Функция-помошник для сортировки массива продуктовых групп
            :param i: экземпляр
            :param l: уровень вложенности
            :return: всего уровней вложенности
            """
            if 'parent' in i and i['parent']:
                return func(i['parent'], l+1)
            return l

        # Сортируем продуктовые группы по уровню вложенности
        deserialized.sort(key=lambda x: func(x, 0))

        for item in deserialized:
            item = self.alter_deserialized_detail_data(request, item)
            bundle = self.build_bundle(data=dict_strip_unicode_keys(item), request=request)
            try:
                group = ProductGroup.objects.get(code=bundle.data['code'])
                self.obj_update(bundle, pk=group.pk, code=group.code)
                signals.product_group_changed.send_robust(sender=bundle.obj)
            except ObjectDoesNotExist:
                self.obj_create(bundle, **self.remove_api_resource_names(kwargs))
                signals.product_group_created.send_robust(sender=bundle.obj)

        return http.HttpCreated(location=self.get_resource_uri())


class ProductCategoryResource(ModelResource):
    class Meta:
        queryset = ProductCategory.objects.all()
        resource_name = 'product_category'
        authentication = MultiAuthentication(BasicAuthentication(), SessionAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]
        filtering = {
            "name": ALL,
            "code": ALL,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    code = fields.CharField(attribute='code', help_text=_('Code'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<code>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.code
        else:
            kwargs['pk'] = bundle_or_obj.code

        return kwargs

    @transaction.atomic
    def obj_update(self, bundle, **kwargs):
        try:
            bundle.obj = ProductCategory.objects.get(code=kwargs['code'])
        except ObjectDoesNotExist:
            raise NotFound('Product category with code={0} not found!'.format(kwargs['code']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        bundle = super(ProductCategoryResource, self).obj_update(bundle, **kwargs)

        # Отправляем сигнал "Определение категории товара изменилось"
        signals.product_category_changed.send_robust(sender=bundle.obj)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            bundle.obj = ProductCategory.objects.get(code=bundle.data['code'])
        except ObjectDoesNotExist:
            pass
        else:
            kwargs['pk'] = bundle.obj.id
            kwargs['code'] = bundle.obj.code
            return self.obj_update(bundle, **kwargs)

        bundle = super(ProductCategoryResource, self).obj_create(bundle, **kwargs)

        # Отправляем сигнал "Создана новая категория товара"
        signals.product_category_created.send_robust(sender=bundle.obj)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    def obj_delete(self, bundle, **kwargs):
        try:
            bundle.obj = ProductCategory.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj, **{'is_removing': True})
        bundle.obj.delete()


class ProductResource(ModelResource):
    class Meta:
        queryset = Product.objects.filter(is_synchronized=True)
        resource_name = 'product'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', 'is_synchronized', ]
        filtering = {
            "name": ALL,
            "sku": ALL,
            "consignment_country": ALL,
            "product_type": ALL,
            "serial_mark": ALL,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    sku = fields.CharField(attribute='sku', unique=True, help_text=_('SKU'))
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', help_text=_('Last modify date'))
    manufacturer = fields.CharField(attribute='manufacturer', null=True, blank=True, help_text=_('Manufacturer'))
    manufacturer_code = fields.CharField(attribute='manufacturer_code', help_text=_('Manufacturer code'), null=True,
                                         blank=True)
    consignment_country = fields.ToOneField("elastic.country.api.CountryResource", attribute='consignment_country',
                                            null=True, blank=True, full=True, help_text=_('Country of consignment'))
    uom = fields.ToOneField(UomResource, attribute='uom', full=True, help_text=_('Unit of Measures'))
    product_type = fields.CharField(attribute='product_type', default='storable', help_text=_('Product type'))
    serial_mark = fields.CharField(attribute='serial_mark', default='accounted_party', help_text=_('Serial mark'))
    list_price = fields.DecimalField(attribute='list_price', default=Decimal("0.0"), help_text=_('List price'))
    cost_price = fields.DecimalField(attribute='cost_price', default=Decimal("0.0"), help_text=_('Cost price'))
    cost_method = fields.CharField(attribute='cost_method', default='fixed', null=True, blank=True,
                                   help_text=_('Serial mark'))
    short_desc = fields.CharField(attribute='short_desc', null=True, blank=True, default='',
                                  help_text=_('Short description'))
    category = fields.ToOneField(to=ProductCategoryResource, attribute='category', null=True, blank=True, full=True,
                                 help_text=_('Category'))
    group = fields.ToOneField(to=ProductGroupResource, attribute='group', null=True, blank=True, full=True,
                              help_text=_('Group'))
    additional_features = fields.ToManyField('elastic.product.api.AdditionalFeatureResource', 'additional_features',
                                             related_name='product', full=True, null=True, blank=True,
                                             help_text=_('Additional features'))
    sync = fields.BooleanField(null=True,  blank=True, default=True)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<sku>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.sku
        else:
            kwargs['pk'] = bundle_or_obj.sku

        return kwargs

    @staticmethod
    def dehydrate_category(bundle):
        if bundle.obj.category:
            return bundle.obj.category.code
        return ''

    @staticmethod
    def hydrate_category(bundle):
        bundle.data['category'] = bundle.data.get('category', None)
        bundle.data['category'] = None if not bundle.data['category'] else bundle.data['category']

        if not bundle.data['category']:
            return bundle

        # Если указан код категории, то ищем запись по справочнику
        if type(bundle.data.get('category', None)) == unicode or type(bundle.data.get('category', None)) == str:
            try:
                bundle.obj.category = ProductCategory.objects.get(code=bundle.data['category'])
                bundle.data.pop('category')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} Category code: {1}. SKU: {2}".format(
                    str(e), bundle.data['category'], bundle.data['sku']))
            except ValueError as e:
                raise BadRequest("{0} Category code: {1}. SKU: {2}".format(
                    str(e), bundle.data['category'], bundle.data['sku']))
        if type(bundle.data.get('category', None)) == dict:
            try:
                bundle.obj.category = ProductCategory.objects.get(code=bundle.data['category']['code'])
                bundle.data.pop('category')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} Category code: {1}. SKU: {2}".format(
                    str(e), bundle.data['category']['code'], bundle.data['sku']))
            except ValueError as e:
                raise BadRequest("{0} Category code: {1}. SKU: {2}".format(
                    str(e), bundle.data['category']['code'], bundle.data['sku']))

        return bundle

    @staticmethod
    def dehydrate_group(bundle):
        if bundle.obj.category:
            return bundle.obj.group.code
        return ''

    @staticmethod
    def hydrate_group(bundle):
        bundle.data['group'] = bundle.data.get('group', None)
        bundle.data['group'] = None if not bundle.data['group'] else bundle.data['group']

        if not bundle.data['group']:
            return bundle

        # Если указан код группы, то ищем запись по справочнику
        if type(bundle.data.get('group', None)) == unicode or type(bundle.data.get('group', None)) == str:
            try:
                bundle.obj.group = ProductGroup.objects.get(code=bundle.data['group'])
                bundle.data.pop('group')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} Group code: {1}. SKU: {2}".format(
                    str(e), bundle.data['group'], bundle.data['sku']))
            except ValueError as e:
                raise BadRequest("{0} Group code: {1}. SKU: {2}".format(
                    str(e), bundle.data['group'], bundle.data['sku']))
        if type(bundle.data.get('group', None)) == dict:
            try:
                bundle.obj.group = ProductGroup.objects.get(code=bundle.data['group']['code'])
                bundle.data.pop('group')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} Group code: {1}. SKU: {2}".format(
                    str(e), bundle.data['group']['code'], bundle.data['sku']))
            except ValueError as e:
                raise BadRequest("{0} Group code: {1}. SKU: {2}".format(
                    str(e), bundle.data['group']['code'], bundle.data['sku']))
        return bundle

    @staticmethod
    def hydrate_consignment_country(bundle):
        # Если страна происхождения не указана, то выставляем "Страна не указана"
        bundle.data['consignment_country'] = '1' if 'consignment_country' not in bundle.data \
            else bundle.data['consignment_country']
        bundle.data['consignment_country'] = '1' if bundle.data['consignment_country'] == '' \
            else bundle.data['consignment_country']

        # Если указан код страны происхождения, то ищем запись по справочнику
        if type(bundle.data['consignment_country']) == unicode or type(bundle.data['consignment_country']) == str:
            try:
                bundle.obj.consignment_country = Country.objects.get(code=bundle.data['consignment_country'])
                bundle.data.pop('consignment_country')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} Сonsignment country code: {1}. SKU: {2}".format(
                    str(e), bundle.data['consignment_country'], bundle.data['sku']))
            except ValueError as e:
                raise BadRequest("{0} Сonsignment country code: {1}. SKU: {2}".format(
                    str(e), bundle.data['consignment_country'], bundle.data['sku']))
        return bundle

    @staticmethod
    def dehydrate_consignment_country(bundle):
        if bundle.obj.consignment_country:
            return str(bundle.obj.consignment_country.code)
        else:
            # Код "1" - "Страна не указана"
            return "1"

    @staticmethod
    def hydrate_uom(bundle):
        if 'uom' not in bundle.data:  # Если единицы измерения не указаны - выставляем штуки
            bundle.obj.uom = Uom.objects.get(code='796')
            return bundle

        # Если указан код единиц измерения, то ищем запись по справочнику
        if type(bundle.data['uom']) == unicode or type(bundle.data['uom']) == str:
            try:
                bundle.obj.uom = Uom.objects.get(code=bundle.data['uom'])
                bundle.data.pop('uom')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0} uom code: {1}. SKU: {2}".format(
                    str(e), bundle.data['uom'], bundle.data['sku']))
        return bundle

    @staticmethod
    def dehydrate_uom(bundle):
        return bundle.obj.uom.code

    @transaction.atomic
    def obj_create(self, bundle, **kwargs):
        try:
            product = Product.objects.get(sku=bundle.data['sku'])
            kwargs['pk'] = product.id
            kwargs['sku'] = product.sku
            return self.obj_update(bundle, **kwargs)
        except ObjectDoesNotExist:
            pass

        bundle = super(ProductResource, self).obj_create(bundle, **kwargs)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    @transaction.atomic
    def obj_update(self, bundle, **kwargs):
        try:
            bundle.obj = Product.objects.get(sku=kwargs['sku'])
        except ObjectDoesNotExist:
            raise NotFound('Product with SKU={0} not found!'.format(kwargs['sku']))

        # Если дата последней модификации сохранненого объекта страше даты последней модификации обновления...
        if 'last_modify_date' not in bundle.data:
            bundle.data['last_modify_date'] = timezone.now().isoformat()
        last_modify_date = make_aware(parse(bundle.data['last_modify_date']))
        if bundle.obj.last_modify_date >= last_modify_date:
            # ....то молча игнорируем обновление
            return bundle

        # Очищаем дополнительные параметры, - они будут заменены обновлением
        for f in bundle.obj.additional_features.iterator():
            f.delete()

        bundle = super(ProductResource, self).obj_update(bundle, **kwargs)

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send_robust(sender=bundle.obj)

        return bundle

    def obj_delete(self, bundle, **kwargs):
        try:
            bundle.obj = Product.objects.get(**kwargs)
        except ObjectDoesNotExist:
            return

        if bundle.data.get('sync', True):
            # Запрашиваем репликацию
            signals.replication_request.send(sender=bundle.obj, **{'is_removing': True})
        bundle.obj.delete()


class AdditionalFeatureResource(ModelResource):
    class Meta:
        queryset = ProductAdditionalFeature.objects.all()
        resource_name = 'product_feature'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]
        filtering = {
            "name": ALL,
            "value": ALL,
            "uom": ALL_WITH_RELATIONS,
            "product": ALL_WITH_RELATIONS,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    uom = fields.ToOneField(UomResource, attribute='uom', full=True, help_text=_('Unit of Measures'))
    value = fields.CharField(attribute='value', help_text=_('Value'))
    product = fields.ToOneField(ProductResource, 'product', help_text=_('Product'))

    def dehydrate(self, bundle):
        bundle.data.pop('product')
        return bundle

    @staticmethod
    def dehydrate_uom(bundle):
        return bundle.obj.uom.code

    @staticmethod
    def hydrate_uom(bundle):
        if 'uom' not in bundle.data:  # Если единицы измерения не указаны - выставляем штуки
            bundle.obj.uom = Uom.objects.get(code='796')
            return bundle

        # Если указан код единиц измерения, то ищем запись по справочнику
        if type(bundle.data['uom']) == unicode or type(bundle.data['uom']) == str:
            try:
                bundle.obj.uom = Uom.objects.get(code=bundle.data['uom'])
                bundle.data.pop('uom')
            except ObjectDoesNotExist as e:
                raise BadRequest(e)
        return bundle

# -*- coding: utf-8 -*-

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


class ProductNotExists(Exception):
    def __init__(self, sku):
        self.sku = sku
        super(ProductNotExists, self).__init__()

    def __unicode__(self):
        return "Product with sku #{0} does not exists!".format(self.sku)

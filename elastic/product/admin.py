# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *
from mpttadmin.admin import MpttAdmin
from forms import ProductGroupForm, ProductAdditionalFeatureForm, ProductForm
import signals
from django.utils.translation import ugettext_lazy as _

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


class UomAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'symbol', 'international_symbol', 'form', )
    list_display_links = ('name',)

admin.site.register(Uom, UomAdmin)


def require_removal_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item, **{'is_removing': True})
require_removal_replication.short_description = _('Distributed request on removal')


def require_replication(modeladmin, request, queryset):
    for item in queryset.iterator():
        signals.replication_request.send(sender=item)
require_replication.short_description = _('Distributed request on update')


class ProductGroupAdmin(MpttAdmin):
    class Meta:
        model = ProductGroup

    tree_title_field = 'name'
    tree_display = ('name', 'code', 'last_modify_date', )
    form = ProductGroupForm

    actions = [require_replication, require_removal_replication, ]

    fieldsets = (
        (None, {
            'fields':  ['parent', 'code', 'name', ]
        }),
    )

admin.site.register(ProductGroup, ProductGroupAdmin)


class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'last_modify_date', )
    list_display_links = ('code',)

    actions = [require_replication, require_removal_replication, ]

    fieldsets = (
        (None, {
            'fields':  ['code', 'name', ]
        }),
    )

admin.site.register(ProductCategory, ProductCategoryAdmin)


class AdditionalFeatureInline(admin.TabularInline):
    model = ProductAdditionalFeature
    form = ProductAdditionalFeatureForm


class ProductAdmin(admin.ModelAdmin):
    list_display = ('sku', 'name', 'manufacturer', 'last_modify_date', )
    list_display_links = ('sku',)
    list_filter = ('group', 'category', 'manufacturer', 'is_synchronized', )
    form = ProductForm
    search_fields = ('name', 'sku', 'manufacturer', )

    actions = [require_replication, require_removal_replication, ]

    fieldsets = (
        (None, {
            'fields':  ['name', 'sku', 'manufacturer', 'manufacturer_code', 'consignment_country', 'uom',
                        'product_type', 'serial_mark', 'short_desc',
                        'group', 'category', 'is_synchronized', ]
        }),
        ('Prices', {
            'classes': ('collapse',),
            'fields':  ['list_price', 'cost_price', 'cost_method', ]
        }),
    )

    inlines = [
        AdditionalFeatureInline,
    ]

admin.site.register(Product, ProductAdmin)

# -*- coding: utf-8 -*-

from autocomplete_light import forms
from models import *
import django

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


class ProductGroupForm(forms.ModelForm):
    class Meta:
        model = ProductGroup
        if django.VERSION >= (1, 6):
            fields = '__all__'


class ProductAdditionalFeatureForm(forms.ModelForm):
    class Meta:
        model = ProductAdditionalFeature
        if django.VERSION >= (1, 6):
            fields = '__all__'


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        if django.VERSION >= (1, 6):
            fields = '__all__'

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('country', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1024, verbose_name='Name')),
                ('sku', models.CharField(unique=True, max_length=128, verbose_name='SKU', db_index=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('manufacturer', models.CharField(max_length=1024, null=True, verbose_name='Manufacturer', blank=True)),
                ('manufacturer_code', models.CharField(max_length=128, null=True, verbose_name='Manufacturer code', blank=True)),
                ('product_type', models.CharField(default=b'storable', max_length=12, verbose_name='Product type', choices=[(b'storable', 'Storable'), (b'consumable', 'Consumable'), (b'service', 'Service')])),
                ('serial_mark', models.CharField(default=b'accounted_party', max_length=16, verbose_name='Serial mark', choices=[(b'serial_number', 'Serial number'), (b'stock_number', 'Stock number'), (b'accounted_party', 'Accounted of the party'), (b'none', 'Quantitative account'), (b'product_party', 'Product party')])),
                ('list_price', models.DecimalField(null=True, verbose_name='List price', max_digits=15, decimal_places=2, blank=True)),
                ('cost_price', models.DecimalField(null=True, verbose_name='Cost price', max_digits=15, decimal_places=2, blank=True)),
                ('cost_method', models.CharField(default=b'fixed', choices=[(b'fixed', 'Fixed cost'), (b'average', 'Average')], max_length=12, blank=True, null=True, verbose_name='Cost method')),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('is_synchronized', models.BooleanField(default=True, verbose_name='Is synchronized')),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='ProductAdditionalFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('product', models.ForeignKey(related_name='additional_features', to='product.Product')),
            ],
            options={
                'verbose_name': 'Additional feature',
                'verbose_name_plural': 'Additional features',
            },
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
            ],
            options={
                'verbose_name': 'Product category',
                'verbose_name_plural': 'Product categories',
            },
        ),
        migrations.CreateModel(
            name='ProductGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=128, verbose_name='Code', db_index=True)),
                ('last_modify_date', models.DateTimeField(verbose_name='Last modify date')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='Parent', blank=True, to='product.ProductGroup', null=True)),
            ],
            options={
                'verbose_name': 'Product Group',
                'verbose_name_plural': 'Product Groups',
            },
        ),
        migrations.CreateModel(
            name='Uom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('code', models.CharField(max_length=16, verbose_name='Code', db_index=True)),
                ('symbol', models.CharField(max_length=56, verbose_name='Symbol')),
                ('international_symbol', models.CharField(max_length=128, verbose_name='International symbol')),
                ('form', models.CharField(max_length=128, verbose_name='Form')),
            ],
            options={
                'verbose_name': 'Unit of Measures',
                'verbose_name_plural': 'Units of Measures',
            },
        ),
        migrations.CreateModel(
            name='UomCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name', db_index=True)),
            ],
            options={
                'verbose_name': 'UOM category',
                'verbose_name_plural': 'UOM category',
            },
        ),
        migrations.AddField(
            model_name='uom',
            name='category',
            field=models.ForeignKey(verbose_name='Category', to='product.UomCategory'),
        ),
        migrations.AddField(
            model_name='productadditionalfeature',
            name='uom',
            field=models.ForeignKey(verbose_name='Unit of measures', blank=True, to='product.Uom', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(verbose_name='Product category', blank=True, to='product.ProductCategory', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='consignment_country',
            field=models.ForeignKey(verbose_name='Country of consignment', blank=True, to='country.Country', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='group',
            field=models.ForeignKey(verbose_name='Product group', blank=True, to='product.ProductGroup', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='uom',
            field=models.ForeignKey(verbose_name='Unit of measures', to='product.Uom'),
        ),
    ]

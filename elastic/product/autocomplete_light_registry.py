# -*- coding: utf-8 -*-

import autocomplete_light.shortcuts as autocomplete_light
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


autocomplete_light.register(
    Uom,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Ед. изм....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)

autocomplete_light.register(
    Country,
    search_fields=['name', ],
    attrs={
        'placeholder': 'Страна происхождения....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)

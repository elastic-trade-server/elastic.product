# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from elastic.country.models import Country
from django.utils import timezone

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product package"


class UomCategory(models.Model):
    """
    Категория единиц измерения
    """
    class Meta:
        verbose_name = _("UOM category")
        verbose_name_plural = _("UOM category")

    #: Наименование
    name = models.CharField(verbose_name=_('Name'), max_length=255, db_index=True)

    def __unicode__(self):
        return self.name


class Uom(models.Model):
    """
    Единицы измерения
    """
    class Meta:
        verbose_name = _("Unit of Measures")
        verbose_name_plural = _("Units of Measures")

    #: Наименование
    name = models.CharField(verbose_name=_('Name'), max_length=255)

    #: Код
    code = models.CharField(max_length=16, verbose_name=_('Code'), db_index=True)

    #: Символическое наименование
    symbol = models.CharField(verbose_name=_('Symbol'), max_length=56)

    #: Интернациональное символическое наименование
    international_symbol = models.CharField(max_length=128, verbose_name=_('International symbol'))

    #: Форма
    form = models.CharField(verbose_name=_('Form'), max_length=128)

    #: Категория. Ссылка на элементсправочника "Категория единиц измерения"
    category = models.ForeignKey(UomCategory, verbose_name=_('Category'))

    def __unicode__(self):
        return self.name


class ProductGroup(MPTTModel):
    """
    Товарная группа
    """
    class Meta:
        verbose_name = _('Product Group')
        verbose_name_plural = _('Product Groups')

    class MPTTMeta:
        parent_attr = 'parent'

    #: Родительская группа
    parent = TreeForeignKey('self', verbose_name=_('Parent'), null=True, blank=True, related_name='children')

    #: Наименование группы
    name = models.CharField(verbose_name=_('Name'), max_length=255)

    #: Код группы
    code = models.CharField(verbose_name=_('Code'), max_length=128, db_index=True)

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(verbose_name=_('Last modify date'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.last_modify_date:
            self.last_modify_date = timezone.now()

        super(ProductGroup, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return self.name

mptt.register(ProductGroup)


class ProductCategory(models.Model):
    """
    Категория товара
    """
    class Meta:
        verbose_name = _('Product category')
        verbose_name_plural = _('Product categories')

    #: Наименование категории
    name = models.CharField(verbose_name=_('Name'), max_length=255)

    #: Код категории
    code = models.CharField(verbose_name=_('Code'), max_length=128, db_index=True)

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(verbose_name=_('Last modify date'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.last_modify_date:
            self.last_modify_date = timezone.now()

        super(ProductCategory, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    """
    Складская карточка (товар)
    """
    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    PRODUCT_TYPE = (
        ('storable', _('Storable')),        # Складируемые
        ('consumable', _('Consumable')),    # Расходные
        ('service', _('Service'))           # Услуга
    )
    PRICE_METHOD = (
        ('fixed', _('Fixed cost')),      # Фиксированная цена
        ('average', _('Average'))   # Средняя цена
    )
    SERIAL_MARK = (
        ('serial_number', _('Serial number')),  # учет по серийным номерам
        ('stock_number', _('Stock number')),  # учет по инвентарным номерам (генерируются автоматически)
        ('accounted_party', _('Accounted of the party')),  # Учитываемые партии.
                                                           # При каждом перемещении указывается номер партии
        ('none', _('Quantitative account')),  # Признак отсутствует. Только количественный учет
        ('product_party', _('Product party'))  # Партионный учет. Списание из партии автоматическое
    )

    #: Наименование товара
    name = models.CharField(verbose_name=_('Name'), max_length=1024)

    #: Складской идентификатор товара
    sku = models.CharField(verbose_name=_('SKU'), unique=True, max_length=128, db_index=True)

    #: Дата/время последней модификации
    last_modify_date = models.DateTimeField(verbose_name=_('Last modify date'))

    #: Производитель товара
    manufacturer = models.CharField(verbose_name=_('Manufacturer'), max_length=1024, null=True, blank=True)

    #: Код производителя товара (артикул)
    manufacturer_code = models.CharField(verbose_name=_('Manufacturer code'), max_length=128, null=True, blank=True)

    #: Страна происхождения. Ссылка на элемент справочника "Страны мира"
    consignment_country = models.ForeignKey(Country, null=True, blank=True, verbose_name=_('Country of consignment'))

    #: Единица измерения. Ссылка на элемент справочника "Единицы измерения"
    uom = models.ForeignKey(Uom, verbose_name=_("Unit of measures"))

    #: Тип товара
    product_type = models.CharField(max_length=12, choices=PRODUCT_TYPE, default='storable',
                                    verbose_name=_('Product type'))

    #: Серийный признак
    serial_mark = models.CharField(max_length=16, choices=SERIAL_MARK, default='accounted_party',
                                   verbose_name=_('Serial mark'))

    #: Учетная цена
    list_price = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True,
                                     verbose_name=_('List price'))

    #: Цена приобретения
    cost_price = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True,
                                     verbose_name=_('Cost price'))

    #: Затратный метод
    cost_method = models.CharField(max_length=12, choices=PRICE_METHOD, default='fixed', null=True, blank=True,
                                   verbose_name=_("Cost method"))

    #: Краткое описание
    short_desc = models.CharField(null=True, blank=True, max_length=255, verbose_name=_('Short description'))

    #: Товарная группа. Ссылка на элемент справочника "Товарные группы"
    group = models.ForeignKey(ProductGroup, null=True, blank=True, verbose_name=_('Product group'))

    #: Товарная категория. Ссылка на элемент справочника "Товарные категории"
    category = models.ForeignKey(ProductCategory, null=True, blank=True, verbose_name=_('Product category'))
    is_synchronized = models.BooleanField(default=True, verbose_name=_('Is synchronized'))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.last_modify_date:
            self.last_modify_date = timezone.now()

        super(Product, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return self.name


class ProductAdditionalFeature(models.Model):
    """
    Дополнительные характеристики товара
    """
    class Meta:
        verbose_name = _('Additional feature')
        verbose_name_plural = _('Additional features')

    #: Наименование товара
    name = models.CharField(verbose_name=_('Name'), max_length=128)

    #: Единица измерения. Ссылка на элемент справочника "Единицы измерения"
    uom = models.ForeignKey(Uom, null=True, blank=True, verbose_name=_("Unit of measures"))

    #: Значение
    value = models.CharField(verbose_name=_('Value'), max_length=255)

    #: Ссылка на родительский справочник
    product = models.ForeignKey(Product, related_name='additional_features')

    def __unicode__(self):
        return u"{0}: {1}".format(self.name, self.value)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.product.save()
        super(ProductAdditionalFeature, self).save(force_insert, force_update, using, update_fields)
